import java.sql.*;

public class Main {

    private static Connection connection;
    private static Statement stmt;


    public static void connect() throws Exception {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:students.db");
        stmt = connection.createStatement();
    }

    public static void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void createTable() throws SQLException {
        stmt.executeUpdate("CREATE TABLE students (ID INTEGER PRIMARY KEY AUTOINCREMENT, Name VARCHAR, Score INT, Email VARCHAR)");
    }

    public static void insertStudents() throws SQLException {
        stmt.executeUpdate("INSERT INTO students(Name, Score,Email) VALUES ('Bob', 95, 'bob@mail.ru'),('Jack', 100, 'jack@mail.ru'),('Kate', 75, 'kate@mail.ru')");
    }

    public static void getStudents() throws SQLException {
        ResultSet rs = stmt.executeQuery("SELECT FROM * students");
        while (rs.next()) {
            String name = rs.getString("Name");
            int score = rs.getInt("Score");
            String email = rs.getString("Email");
            System.out.println("Student " + name + "received " + score + "points, email for communication: " + email);
        }
    }

    public static void deleteStudent() throws SQLException {
        int rows = stmt.executeUpdate("DELETE FROM students WHERE ID=3");
        System.out.printf("%d row(s) deleted", rows);
    }

    public static void dropTable() throws SQLException {
        stmt.executeUpdate("DROP TABLE students");
    }


    public static void main(String[] args) {
        try {
            connect();
            createTable();
            insertStudents();
            getStudents();
            //deleteStudent();
            //dropTable();


        } catch (Exception e) {
            e.printStackTrace();
        }
        disconnect();

    }
}

